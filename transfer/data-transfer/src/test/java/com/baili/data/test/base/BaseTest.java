package com.baili.data.test.base;

import com.baili.data.transfer.DataTransfer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Created by pengshuo on 2019/1/7 15:23
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DataTransfer.class)
@AutoConfigureMockMvc
public class BaseTest {

    public static final Logger logger = LogManager.getLogger(BaseTest.class);

    @Autowired
    public MockMvc mvc;

    @Before
    public void start(){
        logger.info("-----------start---------");
    }

    @Before
    public void over(){
        logger.info("-----------over---------");
    }
}
