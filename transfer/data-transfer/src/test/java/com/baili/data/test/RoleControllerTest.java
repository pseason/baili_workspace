package com.baili.data.test;

import com.baili.data.test.base.BaseTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by pengshuo on 2019/1/12 17:49
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
public class RoleControllerTest extends BaseTest {

    @Test
    public void test() throws Exception {
        logger.info(
                mvc.perform(MockMvcRequestBuilders.get("/test/role").accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString()
        );
    }
}
