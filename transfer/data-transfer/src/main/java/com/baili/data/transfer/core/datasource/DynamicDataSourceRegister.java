package com.baili.data.transfer.core.datasource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pengshuo on 2019/1/10 10:38
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
public class DynamicDataSourceRegister implements ImportBeanDefinitionRegistrar, EnvironmentAware {

    private final static Logger logger = LogManager.getLogger(DynamicDataSourceRegister.class);
    /**指定默认数据源配置*/
    private static final String DATASOURCE_TYPE_DEFAULT = "com.zaxxer.hikari.HikariDataSource";
    /** 默认数据源*/
    private static final String DEFAULT_DATASOURCE = "defaultDataSource";
    /**自定义数据源*/
    public final static Map<String, DataSource> allDataSource = new HashMap<>(6);

    /**
     * 初始化数据源
     * @param environment
     */
    @Override
    public void setEnvironment(Environment environment) {
        initDataSource(environment);
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        logger.info("Dynamic DataSource Registry start...");
        //添加数据源key
        allDataSource.keySet().stream().forEach(key-> DynamicDataSourceContextHolder.dataSourceIds.add(key));
        //创建 DynamicDataSource
        GenericBeanDefinition definition = new GenericBeanDefinition();
        definition.setBeanClass(DynamicDataSource.class);
        definition.setSynthetic(true);
        MutablePropertyValues values = definition.getPropertyValues();
        values.addPropertyValue("defaultTargetDataSource", allDataSource.get(DEFAULT_DATASOURCE));
        values.addPropertyValue("targetDataSources", allDataSource);
        //注册 BeanDefinitionRegistry
        beanDefinitionRegistry.registerBeanDefinition("dataSource", definition);
        logger.info("Dynamic DataSource Registry over...");
    }

    /**
     * 读取配置文件初始化数据源
     * @param env
     */
    private void initDataSource(Environment env) {
        Arrays.stream(TargetDataSource.Target.values()).forEach(one -> {
            String key = one.getName();
            Map<String, Object> map = new HashMap<>(4);
            map.put("driver", env.getProperty(String.format("spring.%s.driver-class-name",key)));
            map.put("url", env.getProperty(String.format("spring.%s.url",key)));
            map.put("username", env.getProperty(String.format("spring.%s.username",key)));
            map.put("password", env.getProperty(String.format("spring.%s.password",key)));
            allDataSource.put(key, buildDataSource(map));
        });
    }

    /**
     * 添加并切换数据源
     * @param key
     * @param dataSource
     */
    public static void dynamicDataSource(String key,DataSource dataSource){
        // put
        allDataSource.put(key,dataSource);
        // holder add
        DynamicDataSourceContextHolder.dataSourceIds.add(key);
        // dynamic set
        DynamicDataSourceContextHolder.setDataSourceType(key);
    }

    /**
     * build datasource
     * @param dataSourceMap
     * @return
     */
    public static DataSource buildDataSource(Map<String, Object> dataSourceMap) {
        try {
            Object type = dataSourceMap.get("type");
            if (type == null) {
                /** 默认DataSource */
                type = DATASOURCE_TYPE_DEFAULT;
            }
            Class<? extends DataSource> dataSourceType;
            dataSourceType = (Class<? extends DataSource>) Class.forName((String) type);
            String driverClassName = dataSourceMap.get("driver").toString();
            String url = dataSourceMap.get("url").toString();
            String username = dataSourceMap.get("username").toString();
            String password = dataSourceMap.get("password").toString();
            // 自定义DataSource配置
            DataSourceBuilder factory = DataSourceBuilder.create()
                    .driverClassName(driverClassName)
                    .url(url)
                    .username(username)
                    .password(password)
                    .type(dataSourceType);
            return factory.build();
        } catch (Exception e) {
            logger.error("build datasource error: ",e);
        }
        return null;
    }

}
