package com.baili.data.transfer.web;

import com.baili.data.transfer.data.entity.bg.GameServerCfg;
import com.baili.data.transfer.data.entity.game.Role;
import com.baili.data.transfer.data.entity.pay.Pay;
import com.baili.data.transfer.data.service.bg.GameServerCfgService;
import com.baili.data.transfer.data.service.game.RoleService;
import com.baili.data.transfer.data.service.pay.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pengshuo on 2019/1/10 16:02
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@RestController
@RequestMapping(value = {"test"})
public class DynamicSourceController {

    @Autowired
    private PayService payService;

    @Autowired
    private GameServerCfgService gameServerCfgService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = {"pay"})
    public Pay getRandPay(){
        return payService.getRandPay();
    }

    @RequestMapping(value = {"cfg"})
    public GameServerCfg getRandCfg(){
        return gameServerCfgService.getRandCfg();
    }

    @RequestMapping(value = {"role"})
    public Role getRandRole(){
        return roleService.getRandRole();
    }
}
