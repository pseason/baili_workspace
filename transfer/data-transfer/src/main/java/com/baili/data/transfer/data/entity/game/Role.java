package com.baili.data.transfer.data.entity.game;

import lombok.Data;

/**
 * Created by pengshuo on 2019/1/12 17:25
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Data
public class Role {
    private Integer roleId;
    private String name;
    private String code;
}
