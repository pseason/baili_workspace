package com.baili.data.transfer.data.entity.pay;

import com.baili.data.transfer.data.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Created by pengshuo on 2019/1/10 15:50
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Data
public class Pay extends BaseEntity {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date payTime;
    private String method;
    private Integer serverId;
    private Long lordId;
    private String nick;
    private Integer vip;
    private Integer level;
    private Integer gold;
    private Integer topup;
    private String orderId;
    private String serialId;
    private Integer amount;
    private Integer payId;
    private Integer platNo;
    private Integer childNo;
}
