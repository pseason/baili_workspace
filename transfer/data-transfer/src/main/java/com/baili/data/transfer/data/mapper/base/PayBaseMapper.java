package com.baili.data.transfer.data.mapper.base;

import com.baili.data.transfer.core.datasource.TargetDataSource;

/**
 * Created by pengshuo on 2019/1/10 14:27
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@TargetDataSource(name = TargetDataSource.Target.Pay)
public interface PayBaseMapper {
}
