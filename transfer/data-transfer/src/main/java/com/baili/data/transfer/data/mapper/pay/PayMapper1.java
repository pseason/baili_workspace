package com.baili.data.transfer.data.mapper.pay;

import com.baili.data.transfer.core.datasource.TargetDataSource;
import com.baili.data.transfer.data.entity.pay.Pay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * Created by pengshuo on 2019/1/4 19:04
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Mapper
@TargetDataSource(name = TargetDataSource.Target.Pay)
public interface PayMapper1 {
    /**
     * get rand one pay
     * @return
     */
    @Select("SELECT * FROM r_pay ORDER BY RAND() LIMIT 1")
    Pay getRandPay();

}
