package com.baili.data.transfer;

import com.baili.data.transfer.core.datasource.DynamicDataSourceRegister;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Created by pengshuo on 2019/1/7 15:02
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@SpringBootApplication
@Import(DynamicDataSourceRegister.class)
public class DataTransfer {
    public static void main(String[] args) {
        SpringApplication.run(DataTransfer.class,args);
    }
}
