package com.baili.data.transfer.data.entity.bg;

import com.baili.data.transfer.data.entity.BaseEntity;
import lombok.Data;

/**
 * Created by pengshuo on 2019/1/10 15:54
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Data
public class GameServerCfg extends BaseEntity {
    private String serverId;
    private String dbIp;
    private String dbName;
    private String userName;
    private String password;
}
