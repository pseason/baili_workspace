package com.baili.data.transfer.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pengshuo on 2019/1/7 15:05
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@RestController
public class HelloController {

    @GetMapping
    public String hello(){
        return "instance: data-transfer";
    }

    @GetMapping(value = {"hello"})
    public String sayHello(){
        return "hello world";
    }
}
