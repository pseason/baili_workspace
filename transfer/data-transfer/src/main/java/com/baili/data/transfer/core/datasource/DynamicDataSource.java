package com.baili.data.transfer.core.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;

/**
 * Created by pengshuo on 2019/1/10 10:04
 * <br>Description: 动态数据源
 * <br>Modified By:
 * <br>Version:
 * @see AbstractRoutingDataSource (每执行一次数据库，动态获取DataSource)
 * @author pengshuo
 */
public class DynamicDataSource  extends AbstractRoutingDataSource {

    /**
     * 获取数据源key
     * @return
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceType();
    }

    /**
     * 切换数据源
     * @return
     */
    @Override
    protected DataSource determineTargetDataSource() {
        return DynamicDataSourceRegister.allDataSource.get(DynamicDataSourceContextHolder.getDataSourceType());
    }
}
