package com.baili.data.transfer.data.mapper.bg;

import com.baili.data.transfer.data.entity.bg.GameServerCfg;
import com.baili.data.transfer.data.mapper.base.DefaultBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;


/**
 * Created by pengshuo on 2018/10/23 10:37
 * Description:
 * Modified By:
 * Version:
 *
 * @author pengshuo
 */
@Mapper
public interface GameServerCfgMapper extends DefaultBaseMapper {

    /**
     * get rand one config
     * @return
     */
    @Results({
            @Result(property = "serverId", column = "serverId"),
            @Result(property = "dbIp", column = "dbIp"),
            @Result(property = "dbName", column = "dbName"),
            @Result(property = "userName", column = "userName"),
            @Result(property = "password", column = "password")
    })
    @Select("SELECT serverId,dbIp,dbName,userName,password FROM m_server_cfg_copy_local ORDER BY RAND() LIMIT 1")
    GameServerCfg getRandCfg();

}
