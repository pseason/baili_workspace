package com.baili.data.transfer.core.datasource;

import java.util.HashSet;

/**
 * Created by pengshuo on 2019/1/10 10:09
 * <br>Description: 动态数据源上下文管理
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
public class DynamicDataSourceContextHolder {

    /** 存放当前线程使用的数据源类型信息 */
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<>();

    /** 存放数据源id */
    public final static HashSet<String> dataSourceIds = new HashSet<>();

    /**
     * 设置数据源
     * @param dataSourceType
     */
    public static void setDataSourceType(String dataSourceType){
        contextHolder.set(dataSourceType);
    }

    /**
     * 获取数据源
     * @return
     */
    public static String getDataSourceType() {
        return contextHolder.get();
    }

    /**
     * 清除数据源
     */
    public static void clearDataSourceType() {
        contextHolder.remove();
    }

    /**
     * 判断当前数据源是否存在
     * @param dataSourceId
     * @return
     */
    public static boolean isContainsDataSource(String dataSourceId) {
        return dataSourceIds.contains(dataSourceId);
    }
}
