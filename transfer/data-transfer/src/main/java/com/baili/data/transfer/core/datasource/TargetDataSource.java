package com.baili.data.transfer.core.datasource;

import java.lang.annotation.*;

/**
 * Created by pengshuo on 2019/1/10 10:20
 * <br>Description: 作用于类、接口或者方法上
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TargetDataSource {

    Target name() default Target.Default;

    enum Target {
        Default("defaultDataSource"),
        Pay("pay"),
        ;
        private String name;
        Target(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }
    }
}
