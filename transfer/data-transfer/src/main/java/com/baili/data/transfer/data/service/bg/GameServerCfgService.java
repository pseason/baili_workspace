package com.baili.data.transfer.data.service.bg;

import com.baili.data.transfer.data.entity.bg.GameServerCfg;
import com.baili.data.transfer.data.mapper.bg.GameServerCfgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pengshuo on 2019/1/10 16:01
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Service
public class GameServerCfgService {

    @Autowired
    private GameServerCfgMapper gameServerCfgMapper;

    /**
     * get random
     * @return
     */
    public GameServerCfg getRandCfg(){
        return gameServerCfgMapper.getRandCfg();
    }
}
