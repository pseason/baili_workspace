package com.baili.data.transfer.data.mapper.game;

import com.baili.data.transfer.data.entity.game.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * Created by pengshuo on 2019/1/12 17:26
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Mapper
public interface RoleMapper {

    /**
     * get random role
     * @return
     */
    @Results({
            @Result(property = "roleId", column = "roleId"),
            @Result(property = "name", column = "name"),
            @Result(property = "code", column = "code")
    })
    @Select("SELECT roleId,name,code FROM s_role ORDER BY RAND() LIMIT 1")
    Role getRandRole();
}
