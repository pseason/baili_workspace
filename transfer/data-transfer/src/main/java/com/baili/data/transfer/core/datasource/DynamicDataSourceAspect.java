package com.baili.data.transfer.core.datasource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by pengshuo on 2019/1/10 10:22
 * <br>Description: 动态数据源切面<br>(方法体、本接口、base接口)
 * <br>Modified By:
 * <br>Version:
 * @see Order (varlue=-1,保证在@Transactional之前执行)
 * @author pengshuo
 */
@Aspect
@Order(-1)
@Component
public class DynamicDataSourceAspect {

    private final static Logger logger = LogManager.getLogger(DynamicDataSourceAspect.class);

    /**
     * 改变数据源(针对base接口)
     * @param joinPoint
     */
    @Before("execution(* com.baili.data.transfer.data.mapper.*.*+.*(..))")
    public void changeDataSource(JoinPoint joinPoint) {
        // TargetDataSource直接作用于方法上
        TargetDataSource annotation = ((MethodSignature)joinPoint.getSignature()).getMethod().getAnnotation(TargetDataSource.class);
        if(annotation == null){
            // 未继承base接口 TargetDataSource直接作用于本接口上
            annotation = (TargetDataSource)joinPoint.getSignature().getDeclaringType().getAnnotation(TargetDataSource.class);
        }
        if(annotation == null){
            // 继承base接口
            for (Class<?> cls : joinPoint.getSignature().getDeclaringType().getInterfaces()) {
                annotation = cls.getAnnotation(TargetDataSource.class);
                if (annotation != null) {
                    break;
                }
            }
        }
        String dataSourceId;
        if(annotation == null){
            // 是否主动设置切换数据库类型
            dataSourceId = DynamicDataSourceContextHolder.getDataSourceType();
            if(!StringUtils.isEmpty(dataSourceId)){
                logger.info(String.format("使用数据源：%s",dataSourceId));
                DynamicDataSourceContextHolder.setDataSourceType(dataSourceId);
            }
        }else{
            dataSourceId = annotation.name().getName();
            if (DynamicDataSourceContextHolder.isContainsDataSource(dataSourceId)) {
                logger.info(String.format("使用数据源：%s",dataSourceId));
                DynamicDataSourceContextHolder.setDataSourceType(dataSourceId);
            } else {
                /** joinPoint.getSignature() ：获取连接点的方法签名对象 */
                logger.error(
                        String.format(
                                "数据源不存在,使用默认的数据源 -> %s",
                                dataSourceId,
                                joinPoint.getSignature()
                        )
                );
            }
        }
    }

    /**
     * 清除当前线程数据源(针对base接口)
     */
    @After("execution(* com.baili.data.transfer.data.mapper.*.*+.*(..))")
    public void clearDataSource() {
        DynamicDataSourceContextHolder.clearDataSourceType();
    }
}
