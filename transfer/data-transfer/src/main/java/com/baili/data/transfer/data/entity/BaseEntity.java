package com.baili.data.transfer.data.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by pengshuo on 2019/1/10 15:54
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Data
public class BaseEntity implements Serializable {
}
