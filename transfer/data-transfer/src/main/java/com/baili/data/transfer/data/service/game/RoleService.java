package com.baili.data.transfer.data.service.game;

import com.baili.data.transfer.core.datasource.DynamicDataSourceRegister;
import com.baili.data.transfer.data.entity.bg.GameServerCfg;
import com.baili.data.transfer.data.entity.game.Role;
import com.baili.data.transfer.data.mapper.bg.GameServerCfgMapper;
import com.baili.data.transfer.data.mapper.game.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pengshuo on 2019/1/12 17:28
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Service
public class RoleService {

    @Autowired
    private GameServerCfgMapper gameServerCfgMapper;

    @Autowired
    private RoleMapper roleMapper;

    /**
     * get random role
     * @return
     */
    public Role getRandRole(){
        // 查找目标数据源
        GameServerCfg cfg = gameServerCfgMapper.getRandCfg();
        if(cfg != null){
            Map<String, Object> map = new HashMap<>(4);
            map.put("driver","com.mysql.cj.jdbc.Driver");
            map.put("url",
                    String.format("jdbc:mysql://%s:3306/%s?useUnicode=true&characterEncoding=utf-8",
                        cfg.getDbIp(),cfg.getDbName()
                    )
            );
            map.put("username", cfg.getUserName());
            map.put("password", cfg.getPassword());
            // 切换数据源
            DynamicDataSourceRegister.dynamicDataSource(
                    cfg.getDbName(),
                    DynamicDataSourceRegister.buildDataSource(map)
            );
            // 查找数据
            return roleMapper.getRandRole();
        }
        return null;
    }
}
