package com.baili.data.transfer.data.service.pay;

import com.baili.data.transfer.data.entity.pay.Pay;
import com.baili.data.transfer.data.mapper.pay.PayMapper;
import com.baili.data.transfer.data.mapper.pay.PayMapper1;
import com.baili.data.transfer.data.mapper.pay.PayMapper2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pengshuo on 2019/1/10 16:00
 * <br>Description:
 * <br>Modified By:
 * <br>Version:
 *
 * @author pengshuo
 */
@Service
public class PayService {

    @Autowired
    private PayMapper payMapper;
    @Autowired
    private PayMapper1 payMapper1;
    @Autowired
    private PayMapper2 payMapper2;

    /**
     * get random
     * @return
     */
    public Pay getRandPay(){
        payMapper1.getRandPay();
        payMapper2.getRandPay();
        return payMapper.getRandPay();
    }
}
